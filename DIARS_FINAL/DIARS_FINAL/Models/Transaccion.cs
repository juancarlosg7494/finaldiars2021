﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIARS_FINAL.Models
{
    public class Transaccion
    {
        public int Id{ get; set; }
        public int CuentaId { get; set; }
        
        public DateTime Fecha{ get; set; }
        public double Monto{ get; set; }
        public string Tipo { get; set; }
        public string  Descripcion { get; set; }

    }
}
