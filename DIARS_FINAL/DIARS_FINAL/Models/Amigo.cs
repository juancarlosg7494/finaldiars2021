﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIARS_FINAL.Models
{
    public class Amigo
    {
        public int Id { get; set; }
        public int EnlaceId { get; set; }
        public int UsuarioId { get; set; }
        public string SolicitudAmistad { get; set; }
        public Usuario Usuario { get; set; }
    }
}
