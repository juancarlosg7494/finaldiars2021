﻿using DIARS_FINAL.DB;
using DIARS_FINAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIARS_FINAL.Controllers
{


    public class CuentaController : Controller
    {
        private AppDiarsFinalContext context;
        public CuentaController(AppDiarsFinalContext context)
        {
            this.context = context;
        }

        // GET: CuentaController
        public ActionResult Index()
        {
            var usuario = GetUsuario();

            var cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();


            ViewBag.Propio = CalcularSaldoPropio(cuentas);
            ViewBag.Credito = CalcularSaldoCredito(cuentas);
            return View(cuentas);
        }

        
        [HttpGet]
        public ActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CrearCuenta(Cuenta cuenta)
        {
            var usuario = GetUsuario();
            cuenta.UsuarioId = usuario.Id;
            context.Cuentas.Add(cuenta);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Ingresos(int id)
        {
            var cuenta = context.Cuentas.Where(o => o.Id == id).FirstOrDefault();
            var ingresos = context.Transacciones.Where(o => o.CuentaId == cuenta.Id && o.Tipo == "ingreso");
            ViewBag.Cuenta = cuenta;
            return View(ingresos);
        }


        [HttpGet]
        public ActionResult CrearIngreso()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult CrearIngreso(Transaccion ingreso)
        {

            var cuenta = context.Cuentas.Find(ingreso.CuentaId);

            cuenta.Monto = cuenta.Monto + ingreso.Monto;
            ingreso.Tipo = "ingreso";
            context.Transacciones.Add(ingreso);
            context.SaveChanges();
            return RedirectToAction("Ingresos", new { id = ingreso.CuentaId });
        }
        public ActionResult Gastos(int id)
        {

            var cuenta = context.Cuentas.Where(o => o.Id == id).FirstOrDefault();
            var gastos = context.Transacciones.Where(o => o.CuentaId == cuenta.Id && o.Tipo == "gasto");
            ViewBag.Cuenta = cuenta;
            return View(gastos);
        }

        [HttpGet]
        public ActionResult CrearGasto()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View();
        }
        [HttpPost]
        public ActionResult CrearGasto(Transaccion gasto)
        {
            var cuenta = context.Cuentas.Find(gasto.CuentaId);

            if (gasto.Monto <= cuenta.Monto) {
                
                cuenta.Monto = cuenta.Monto - gasto.Monto;
                gasto.Tipo = "gasto";
                context.Transacciones.Add(gasto);
                context.SaveChanges();

                return RedirectToAction("Gastos", new { id = gasto.CuentaId });
            }
            var usuario = GetUsuario();

            ModelState.AddModelError("Monto", "El monto es muy grande");
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            return View();

        }

        [HttpGet]
        public ActionResult TransferenciaCuentas()
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult TransferenciaCuentas(float monto, int idCuentaGasto, int idCuentaIngreso, DateTime fecha)
        {
            var cuentaGasto = context.Cuentas.Find(idCuentaGasto);
            if (monto <= cuentaGasto.Monto) {
                var cuentaIngreso = context.Cuentas.Find(idCuentaIngreso);
                
                cuentaGasto.Monto = cuentaGasto.Monto - monto;
                cuentaIngreso.Monto = cuentaIngreso.Monto + monto;
                context.Transacciones.Add(new Transaccion()
                {
                    CuentaId = idCuentaGasto,
                    Tipo = "gasto",
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia"
                });
                context.Transacciones.Add(new Transaccion()
                {
                    CuentaId = idCuentaIngreso,
                    Tipo = "ingreso",
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia"
                });
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("Monto", "El monto ingresado es mayor al monto de la cuenta");

            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();

            return View(monto);
        }
        
        public ActionResult ListarUsuarios()
        {
            var usuarios = context.Usuarios.ToList();
            ViewBag.Usuario = GetUsuario();
            return View(usuarios);
        }
        public ActionResult EnviarSolicitud(int id)
        {
            var contacto = context.Usuarios.Where(o => o.Id == id).FirstOrDefault();
            var usuario = GetUsuario();

            var amistad = new Amigo()
            {
                UsuarioId = usuario.Id,
                EnlaceId = contacto.Id,
                SolicitudAmistad = "Por Aceptar"
            };
            context.Amigos.Add(amistad);
            context.SaveChanges();
            ViewBag.Usuario = GetUsuario();
            return RedirectToAction("Index");
        }
        public ActionResult AceptarSolicitud(int id)
        {
            var usuario = GetUsuario();
            var solicitud = context.Amigos.Where(o => o.EnlaceId == usuario.Id && o.UsuarioId == id).FirstOrDefault();
            solicitud.SolicitudAmistad = "Aceptada";
            context.SaveChanges();
            return RedirectToAction("ListarAmigos");
        }
        public ActionResult ListarAmigos()
        {
            var usuario = GetUsuario();

            var losQueAcepte = context.Amigos.Where(o => o.EnlaceId == usuario.Id && o.SolicitudAmistad == "Aceptada").ToList();
            var losQueMeAceptaron = context.Amigos.Where(o => o.UsuarioId == usuario.Id && o.SolicitudAmistad == "Aceptada").ToList();

            var MisAmigos = new List<Usuario>();
            var usuarios = context.Usuarios.ToList();


            foreach (var item in losQueAcepte)
            {
                MisAmigos.Add(context.Usuarios.Find(item.UsuarioId));
            }
            foreach (var item in losQueMeAceptaron)
            {
                MisAmigos.Add(context.Usuarios.Find(item.EnlaceId));
            }

            return View(MisAmigos);
        }
        public ActionResult ListarSolicitudes()
        {
            var usuario = GetUsuario();
            var solicitudesAmistad = context.Amigos.Where(o => o.EnlaceId == usuario.Id && o.SolicitudAmistad == "Por Aceptar").ToList();
            var misSolicitudes = new List<Usuario>();
            foreach (var item in solicitudesAmistad)
            {
                misSolicitudes.Add(context.Usuarios.Find(item.UsuarioId));
            }
            return View(misSolicitudes);
        }
        [HttpGet]
        public ActionResult TransferirAmigo(int id)
        {
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ViewBag.cuentasContacto = context.Cuentas.Where(o => o.UsuarioId == id).ToList();

            return View();
        }
        [HttpPost]
        public ActionResult TransferirAmigo(float monto, int idCuentaGasto, int idCuentaIngreso, DateTime fecha)
        {
            var cuentaGasto = context.Cuentas.Find(idCuentaGasto);
            if (monto <= cuentaGasto.Monto)
            {
                var cuentaIngreso = context.Cuentas.Find(idCuentaIngreso);

                cuentaGasto.Monto -= monto;
                cuentaIngreso.Monto +=  monto;
                context.Transacciones.Add(new Transaccion()
                {
                    CuentaId = idCuentaGasto,
                    Tipo = "gasto",
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia con amigo"
                });
                context.Transacciones.Add(new Transaccion()
                {
                    CuentaId = idCuentaIngreso,
                    Tipo = "ingreso",
                    Fecha = fecha,
                    Monto = monto,
                    Descripcion = "Transferencia con amigo"
                });
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            var usuario = GetUsuario();
            ViewBag.cuentas = context.Cuentas.Where(o => o.UsuarioId == usuario.Id).ToList();
            ViewBag.cuentasContacto = context.Cuentas.Where(o => o.Id == idCuentaIngreso).ToList();
            ModelState.AddModelError("Monto", "El monto ingresado es mayor al monto de la cuenta");

            return View();
        }
        
        
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }
        private double CalcularSaldoPropio(List<Cuenta> cuentas)
        {
            double saldoPropio = 0;
            double deuda = 0;

            foreach (var item in cuentas)
            {
                if (item.Categoria == "Propia")
                {
                    saldoPropio = saldoPropio + item.Monto;
                }
                else
                {
                    var gastos = context.Transacciones.Where(o => o.CuentaId == item.Id && o.Tipo == "gasto").ToList();
                    var ingresos = context.Transacciones.Where(o => o.CuentaId == item.Id && o.Tipo == "ingreso").ToList();
                    foreach (var ingreso in ingresos)
                    {
                        deuda = deuda + ingreso.Monto;
                    }
                    foreach (var gasto in gastos)
                    {
                        deuda = deuda - gasto.Monto;
                    }
                }
            }
            if (deuda < 0)
            {
                saldoPropio = saldoPropio + deuda;
            }


            return saldoPropio;
        }
        private double CalcularSaldoCredito(List<Cuenta> cuentas)
        {
            double saldo = 0;
            foreach (var item in cuentas)
            {
                saldo = saldo + item.Monto;

            }
            return saldo;
        }

    }
}
