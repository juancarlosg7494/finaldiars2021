﻿using DIARS_FINAL.DB;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using DIARS_FINAL.Models;

namespace DIARS_FINAL.Controllers
{
    public class AuthController : Controller
    {
        private AppDiarsFinalContext context;
        private IConfiguration configuration;
        public AuthController(AppDiarsFinalContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var hash = CreateHash(password);
            var user = context.Usuarios
                .FirstOrDefault(o => o.Username == username && o.Password == hash);

            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o Password incorrecto";
                HttpContext.Response.StatusCode = 400;

                return View("Login");
            }

            // Autenticar
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Username),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);
            
            return RedirectToAction("Index", "Cuenta");
        }


        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }




        [HttpGet]
        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }


        [HttpGet]
        public ActionResult Registrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrar(Usuario usuario, string PasswordConf)
        {
            var usuarios = context.Usuarios.ToList();
            
            
            if (ModelState.IsValid)
            {
                usuario.Username = usuario.Username;
                usuario.Password = CreateHash(PasswordConf);
                context.Usuarios.Add(usuario);
                context.SaveChanges();
                return RedirectToAction("Login");
            }
            return View("Registrar", usuario);
        }
    }
}
