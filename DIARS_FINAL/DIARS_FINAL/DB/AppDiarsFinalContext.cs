﻿using DIARS_FINAL.DB.Maps;
using DIARS_FINAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIARS_FINAL.DB
{
    public class AppDiarsFinalContext : DbContext
    {
        public DbSet<Usuario> Usuarios{ get; set; }
        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Transaccion> Transacciones { get; set; }
        public DbSet<Amigo> Amigos { get; set; }

        public AppDiarsFinalContext(DbContextOptions<AppDiarsFinalContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new TransaccionMap());
            modelBuilder.ApplyConfiguration(new AmigoMap());

        }
    }
}
